# About me

## Fundamental Values

These are the values and characteristics I follow when doing something
or when I say I like something.

### Simple

When something is simple it doesn't mean that it's easy, but it's easier
for you to get a clear picture of the thing and how to use/achieve it.

### Explicit

When something is explicit everyone has a clear picture of what it is,
no assumptions. It makes things more effective and reduces churn.

### Minimal

Minimalism leads to explicitness and simpleness. It reduces any decision
making and allows you to have a focused goal and things that you love.
Less distractions in life.
